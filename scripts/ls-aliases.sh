#!/usr/bin/env bash
# ls aliases which may be common but not universal

alias ll="ls -al"
alias lt="ls -alt"
alias lr="ls -altr"
