#!/usr/bin/env bash
# Log to terminal in a pretty format which is easy to find.
# Each argument will print as one line, followed by a separater, and then the
# next argument. Quoted strings are treated as one argument. The entire output
# is surrounded by a blank line, then a horizontal separater, then the args.

function pretty_out {
	echo -e "\n==========="
	# Double-quotes prevent splitting string input into words
	for line in "$@"; do
		echo -e $line
		echo -e "==========="
	done
	echo -en "\n"
}
