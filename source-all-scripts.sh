#!/usr/bin/env bash

if [[ "${BASH_SOURCE%/*}" == "$(basename $BASH_SOURCE)" ]]; then
	pushd scripts 1> /dev/null
else
	pushd "${BASH_SOURCE%/*}/scripts" 1> /dev/null
fi

test $? -ne 0 && return $?

for file in $(ls); do
	source $file
done

popd 1> /dev/null
